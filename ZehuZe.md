## Instructions:

* Create a new configuration repository
 - install lab (GitLab CLI https://zaquestion.github.io/lab/)
    curl -s https://raw.githubusercontent.com/zaquestion/lab/master/install.sh | sudo bash
 - create PAT   (https://gitlab.com/profile/personal_access_tokens)
    lab     (use token) 
 - create project
    lab project create -n echo --public -d "final kuberjenkins exercise"
 - add conf into project (git init, add repo git@gitlab.com:labthyneighbor/echo.git, commit, push)


* Set up a K8S cluster on GCP (You can do this using the gcloud CLI. Bonus using Terraform...)
 - (install gcloud, then) initialize a cloud project @@@ enable billing for API usage 
    gcloud init
    gcloud compute project-info add-metadata --metadata google-compute-default-region=europe-west1,google-compute-default-zone=europe-west1-b
 <!-- -  enable APIs
    gcloud services enable compute.googleapis.com
    gcloud services enable servicenetworking.googleapis.com
    gcloud services enable cloudresourcemanager.googleapis.com
    gcloud services enable container.googleapis.com
 - create project, service account
    gcloud iam service-accounts create terraform-gke
    gcloud projects create atom88 --name="Auto-Mating service" --labels=app=echo
 - grant roles to service account
    gcloud projects add-iam-policy-binding atom88 --member serviceAccount:terraform-gke@atom88.iam.gserviceaccount.com --role roles/container.admin
    gcloud projects add-iam-policy-binding atom88 --member serviceAccount:terraform-gke@atom88.iam.gserviceaccount.com --role roles/compute.admin
    gcloud projects add-iam-policy-binding atom88 --member serviceAccount:terraform-gke@atom88.iam.gserviceaccount.com --role roles/iam.serviceAccountUser
    gcloud projects add-iam-policy-binding atom88 --member serviceAccount:terraform-gke@atom88.iam.gserviceaccount.com --role roles/resourcemanager.projectIamAdmin
 - create a key for Terraform to authenticate as the service account
    gcloud iam service-accounts keys create terraform-gke-keyfile.json --iam-account=terraform-gke@atom88.iam.gserviceaccount.com 
- !#!# using terraform-gke-keyfile.json save pvt key to a file (e.g ~/.ssh/gcloud_id_rsa), gen pub:
   chmod 400 ~/.ssh/gcloud_id_rsa
   ssh-keygen -y -f ~/.ssh/gcloud_id_rsa > ~/.ssh/gcloud_id_rsa.pub

     -->

 - create the cluster
    gcloud beta container --project "atom88" clusters create "clust4life" --zone "europe-west1-b" --no-enable-basic-auth --cluster-version "1.15.8-gke.3" --machine-type "n1-standard-2" --image-type "COS" --disk-type "pd-standard" --disk-size "50" --num-nodes "2" --enable-stackdriver-kubernetes --metadata disable-legacy-endpoints=true --scopes "https://www.googleapis.com/auth/devstorage.read_only","https://www.googleapis.com/auth/logging.write","https://www.googleapis.com/auth/monitoring","https://www.googleapis.com/auth/servicecontrol","https://www.googleapis.com/auth/service.management.readonly","https://www.googleapis.com/auth/trace.append" --enable-ip-alias --network "projects/atom88/global/networks/default" --subnetwork "projects/atom88/regions/europe-west1/subnetworks/default" --default-max-pods-per-node "110" --addons HorizontalPodAutoscaling,HttpLoadBalancing --enable-autoupgrade --enable-autorepair
 - set k8s in shell to the cluster
    gcloud container clusters get-credentials clust4life --zone europe-west1-b
 - enable autoscaling of existing cluster:
    gcloud container clusters update clust4life --enable-autoscaling \
    --min-nodes 1 --max-nodes 3 --zone europe-west1-b --node-pool default-pool


* Set up a new standalone Jenkins VM (Not inside Kubernetes). Again - you can use all the tools at your disposal - Terraform, gcloud, Bash, Ansible...
- upload to dockerhub an image of jenkins+docker
   docker login --username dockeric4
   docker tag jenkloud dockeric4/jenkloud
   docker push dockeric4/jenkloud
- (using .tf files  https://gitlab.com/labthyneighbor/tf4echo/-/tree/master)
   terraform init
   terraform plan
   terraform apply
- connect to the instance (project and instance names may differ)
   gcloud compute ssh --zone europe-west1-b --project atom88 tf-docker-0
- - NOTE: this will create a key-pair locally. 


* Clone the Echo app into your own public Github repository
   git clone https://gitlab.com/fatliverfreddy/echo.git
   lab project create -n echo --public -d "Echo Word!"
   mv echo echoapp && cd echoapp && echo "Gotta update something" >> README.md 
   git remote set-url origin git@gitlab.com:labthyneighbor/echoapp.git
   git add . && git commit -m "Echo World!" && git push -u origin master


* In the new git repo, create two new branches: 'dev' and 'staging'
   git checkout -b dev     && git push --set-upstream origin dev
   git checkout -b staging && git push --set-upstream origin staging


* Set up a Jenkins pipeline to build these branches as well as the master branch, and produce Docker images tagged 'dev-${GIT_COMMIT_HASH}',
'staging-${GIT_COMMIT_HASH}', and '1.0.${JENKINS_BUILD_NUMBER}' from these branches respectively
- create key-gen for jenkins connection to GitLab
   ssh-keygen -t ed25519 -C "ic4ftp@gmail.com"
   (add content of) /var/jenkins_home/.ssh/id_ed25519.pub into https://gitlab.com/profile/keys
   (add content of) /var/jenkins_home/.ssh/id_ed25519 into jenkins SSH Agent plugin credentials

- - MBP:
  > scan GitLab branches (must have Jenkinsfile)
  > build docker image, tagged according to branch
  > push to repository (gcr or dockerhub)
   setup Jenkins container credentials with GCR
     gcloud auth list              = checks current credentials used
     gcloud init                   = to change to a service account (so not to use the VM's)
     gcloud auth configure-docker  = adds GCR repos to Docker
  > eventually push to the gcr
     docker push eu.gcr.io/atom88/(tagged img)




* Install Helm to your cluster


* Install an Ingress controller to the cluster
mongodb://user:pass@mongo-mongodb.mongo.mongodb.svc.cluster.local:27017/echo

* Install FluxCD to your cluster
   sudo snap install fluxctl --classic
   git clone https://github.com/fluxcd/helm-operator-get-started
   cd helm-operator-get-started
   helm repo add fluxcd https://charts.fluxcd.io
   helm repo update
   kubectl create ns fluxcd
   helm upgrade -i flux fluxcd/flux \
      --wait --namespace fluxcd \
      --set git.url=git@gitlab.com:labthyneighbor/flux_cd.git
   fluxctl identity --k8s-fwd-ns fluxcd      >> add ssh key to GitLab
   kubectl apply -f https://raw.githubusercontent.com/fluxcd/helm-operator/master/deploy/flux-helm-release-crd.yaml
   helm upgrade -i helm-operator fluxcd/helm-operator --wait \
      --namespace fluxcd \
      --set git.ssh.secretName=flux-git-deploy \
      --set helm.versions=v3
   git remote set-url origin git@gitlab.com:labthyneighbor/flux_cd.git
   git add .
   git commit -m "Initial commit"
   git push -u origin master
   fluxctl sync --k8s-fwd-ns fluxcd



* Set up CD pipelines for the Echo app, to deploy the three Image tags into these separate namespaces: 'dev', 'staging' and 'production'


* Install MongoDB to your your cluster with Authentication enabled
   helm install -n mongodb mongo charts/mongodb -f values/mongodb.yml
   kubectl exec -ti -n mongodb mongo-mongodb-primary-0 bash
   echo $MONGODB_ROOT_PASSWORD
   mongo -u root -p ...(^)
   use echo-staging
   db.createUser({user: "user", pwd: "pass", roles: [ "readWrite", "dbAdmin" ]})

* Save the MongoDB connection URL to the cluster as a Secret


* Add a dynamic configuration in each environment (dev, staging, production) using a configMap and dynamically add the keys as env variables to echo:
- 'dev' should NOT persist to MongoDB
- 'staging' should persist to the DB 'echo-staging'
- 'production' should persist to the DB 'echo'

* Add the MongoDB connection string to the staging and prod deployments as an env variable from the Secret you created

* Expose all envs to the public internet using different subdomains: dev.${YOUR_DOMAINNAME.EXT}, staging.${YOUR_DOMAINNAME.EXT}, and ${YOUR_DOMAINNAME}

* Install the EFK stack to the cluster and collect application logs from all environments.
   helm pull --untar stable/elastic-stack --version 1.8.0
   kubectl apply -f namespaces/logging.yml
   helm install -n logging efk charts/elastic-stack
   kubectl get pods -n logging
   kubectl port-forward efk-kibana-... 5601 -n logging
- - Bear in mind u can enable fluentd instead of the stack's default logstash


* Install the monitoring stack to your cluster
   kubectl apply -f namespaces/monitoring.yml 
   kubectl apply -f charts/prometheus-operator/crds
   helm install --namespace monitoring prometheus charts/prometheus-operator
   kubectl --namespace monitoring port-forward svc/prometheus-prometheus-oper-prometheus 9090 &
   kubectl --namespace monitoring port-forward svc/prometheus-grafana 8080:80 &
   kubectl --namespace monitoring port-forward svc/prometheus-prometheus-oper-alertmanager 9093 &
